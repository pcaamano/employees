
select * from VAULT.EMPLOYEES;

select * from jobs;
select * from departments;



select emp.FIRST_NAME, emp.LAST_NAME, emp.EMAIL, emp.PHONE_NUMBER, emp.HIRE_DATE, j.JOB_TITLE, emp.SALARY, emp.MANAGER_ID, dep.DEPARTMENT_NAME
from EMPLOYEES emp 
join JOBS j
join DEPARTMENTS dep
	on emp.JOb_ID = j.JOB_ID
    and emp.DEPARTMENT_ID = dep.DEPARTMENT_ID
    order by EMPLOYEE_ID;


select emp.FIRST_NAME, emp.LAST_NAME, emp.EMAIL, emp.PHONE_NUMBER, emp.HIRE_DATE, j.JOB_TITLE, emp.SALARY, emp.MANAGER_ID, dep.DEPARTMENT_NAME
from EMPLOYEES emp 
join JOBS j
join DEPARTMENTS dep
	on emp.JOB_ID = j.JOB_ID
    and emp.DEPARTMENT_ID = dep.DEPARTMENT_ID
    where emp.FIRST_NAME LIKE '%m%'
    and emp.LAST_NAME like '%%'
    and emp.EMAIL like '%%'
    and emp.PHONE_NUMBER like '%5411%'
    and j.JOB_TITLE like '%vendedor%'
    and dep.DEPARTMENT_NAME like '%%'
    order by EMPLOYEE_ID;
    
    
    
select dep.DEPARTMENT_NAME,loc.CITY,loc.STATE_PROVINCE,co.COUNTRY_NAME
from VAULT.DEPARTMENTS dep
join VAULT.LOCATIONS loc
join VAULT.COUNTRIES co
	on dep.LOCATION_ID = loc.LOCATION_ID
    and loc.COUNTRY_ID = co.COUNTRY_ID
    order by dep.DEPARTMENT_ID;
    
/*validación que determine el promedio de salario de todos los empleados pertenecientes a todos los Department cuyo LocationId sea el mismo del objeto*/


select sum(emp.SALARY) as SALARIO_SUMADO, count(emp.EMPLOYEE_ID) as CANT_EMPLOYEES, dep.DEPARTMENT_NAME, loc.CITY,loc.STATE_PROVINCE
from VAULT.EMPLOYEES emp
join VAULT.DEPARTMENTS dep
join VAULT.LOCATIONS loc
	on emp.DEPARTMENT_ID = dep.DEPARTMENT_ID
    and dep.LOCATION_ID = loc.LOCATION_ID
    group by dep.DEPARTMENT_ID,loc.LOCATION_ID;

select (sum(emp.SALARY)/count(emp.EMPLOYEE_ID)) as SALARIO_PROMEDIO, count(emp.EMPLOYEE_ID) as CANT_EMPLOYEES, dep.DEPARTMENT_NAME, loc.CITY,loc.STATE_PROVINCE
from VAULT.EMPLOYEES emp
join VAULT.DEPARTMENTS dep
join VAULT.LOCATIONS loc
	on emp.DEPARTMENT_ID = dep.DEPARTMENT_ID
    and dep.LOCATION_ID = loc.LOCATION_ID
    group by dep.DEPARTMENT_ID,loc.LOCATION_ID;
    

select sum(emp.SALARY) as SALARIO_SUMADO, count(emp.EMPLOYEE_ID) as CANT_EMPLOYEES, dep.DEPARTMENT_NAME, loc.CITY,loc.STATE_PROVINCE
from VAULT.EMPLOYEES emp
join VAULT.DEPARTMENTS dep
join VAULT.LOCATIONS loc
	on emp.DEPARTMENT_ID = dep.DEPARTMENT_ID
    and dep.LOCATION_ID = loc.LOCATION_ID
    where emp.DEPARTMENT_ID = 4
    group by dep.DEPARTMENT_ID,loc.LOCATION_ID;
    
    
    
select emp.EMPLOYEE_ID,emp.FIRST_NAME, emp.LAST_NAME, emp.EMAIL, emp.PHONE_NUMBER, emp.HIRE_DATE, j.JOB_TITLE, emp.SALARY, dep.DEPARTMENT_NAME from EMPLOYEES emp join JOBS j join DEPARTMENTS dep on emp.JOb_ID = j.JOB_ID and emp.DEPARTMENT_ID = dep.DEPARTMENT_ID where emp.FIRST_NAME LIKE '%Mariana%' and emp.LAST_NAME like '%%' and emp.EMAIL like '%%' and emp.PHONE_NUMBER like '%%' and j.JOB_TITLE like '%%' and dep.DEPARTMENT_NAME like '%%' order by EMPLOYEE_ID;

