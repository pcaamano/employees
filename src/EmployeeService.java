
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class EmployeeService {
	

	/*** ATTRIBUTES ***/
	private int employeeId;
	private int jobId;
	private int managerId;
	private int departmentId;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private String hireDate;
	private String jobName;
	private String departmentName;
	private double salary;
	private double commission;
	
	
	/*** CONSTRUCTOR ***/
	public EmployeeService() {
			
	}
	
	/*** OVERLOADED CONSTRUCTOR ***/
	public EmployeeService(int idEmployee, String firstName, String lastName, String email, String phoneNumber, String job, String department) {
		this.employeeId = idEmployee;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.jobName = job;
		this.departmentName = department;
	}

	public EmployeeService(String firstName, String lastName, String email, String phoneNumber, String hireDate, int jobId, double salary, double commission, int managerId,  int departmentId) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.hireDate = hireDate;
		this.jobId = jobId;
		this.salary = salary;
		this.commission = commission;
		this.managerId=managerId;
		this.departmentId = departmentId;
	}
	
	public EmployeeService(int idEmployee, String firstName, String lastName, String email, String phoneNumber, String hireDate, int jobId, double salary, double commission, int managerId,  int departmentId) {
		this.employeeId = idEmployee;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.hireDate = hireDate;
		this.jobId = jobId;
		this.salary = salary;
		this.commission = commission;
		this.managerId=managerId;
		this.departmentId = departmentId;
	}
	
	public EmployeeService(int idEmployee) {
		this.employeeId = idEmployee;
	}
	


	/*** METHODS ***/
	
	//Test connection
	public void connectDB() {
		try {
		      Class.forName("com.mysql.jdbc.Driver");
		      Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/VAULT","root","");
		      System.out.println("Conexión a Base de datos exitosa");
		      connection.close();
		    }catch(Exception ex) {
		    	System.out.println("Error de conexión a base de datos");
		    	System.out.println(ex.toString());
		    	ex.printStackTrace();
		    }
	}
	
	
	
	
	//List registrys in Employees table
	public void listEmployees(){
		try {
		      Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/VAULT","root","");
		      Statement statement = connection.createStatement(); 
		      ResultSet results = statement.executeQuery ("select emp.FIRST_NAME, emp.LAST_NAME, emp.EMAIL, emp.PHONE_NUMBER, emp.HIRE_DATE, j.JOB_TITLE, emp.SALARY, dep.DEPARTMENT_NAME from EMPLOYEES emp join JOBS j join DEPARTMENTS dep on emp.JOb_ID = j.JOB_ID and emp.DEPARTMENT_ID = dep.DEPARTMENT_ID order by EMPLOYEE_ID;");
		      
		      // print results
		      while(results.next()==true){
		    	  System.out.println(results.getString(1)+" | "+results.getString(2)+" | "+results.getString(3)+" | "+results.getString(4)+" | "+results.getDate(5)+" | "+results.getString(6)+" | $"+results.getFloat(7)+" | "+results.getString(8));   	  
		      }
		      connection.close();
		    }catch(SQLException ex) {
		    	System.out.println("¡Error!");
		    	System.out.println(ex.toString());
		    	ex.printStackTrace();
		    }	
	}
	
	
	
	//Select registry with condition
	public void findEmployee() {
		try {
		      Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/VAULT","root","");
		      String query="";
		      
		      Statement statement = connection.createStatement(); 
		      if(this.employeeId==0) {
		    	  query="select emp.EMPLOYEE_ID,emp.FIRST_NAME, emp.LAST_NAME, emp.EMAIL, emp.PHONE_NUMBER, emp.HIRE_DATE, j.JOB_TITLE, emp.SALARY, dep.DEPARTMENT_NAME from EMPLOYEES emp join JOBS j join DEPARTMENTS dep on emp.JOb_ID = j.JOB_ID and emp.DEPARTMENT_ID = dep.DEPARTMENT_ID where emp.FIRST_NAME LIKE '%"+this.firstName+"%' and emp.LAST_NAME like '%"+this.lastName+"%' and emp.EMAIL like '%"+this.email+"%' and emp.PHONE_NUMBER like '%"+this.phoneNumber+"%' and j.JOB_TITLE like '%"+this.jobName+"%' and dep.DEPARTMENT_NAME like '%"+this.departmentName+"%' order by EMPLOYEE_ID;";
		      }else {
		    	  query="select emp.EMPLOYEE_ID,emp.FIRST_NAME, emp.LAST_NAME, emp.EMAIL, emp.PHONE_NUMBER, emp.HIRE_DATE, j.JOB_TITLE, emp.SALARY, dep.DEPARTMENT_NAME from EMPLOYEES emp join JOBS j join DEPARTMENTS dep on emp.JOb_ID = j.JOB_ID and emp.DEPARTMENT_ID = dep.DEPARTMENT_ID where emp.EMPLOYEE_ID = "+this.employeeId+" order by EMPLOYEE_ID;";
		      }
		      ResultSet results = statement.executeQuery(query);
		      
		      // print results
		      while(results.next()==true) {
		    	    System.out.println (results.getInt(1)+" | "+results.getString(2)+" | "+results.getString(3)+" | "+results.getString(4)+" | "+results.getString(5)+" | "+results.getDate(6)+" | "+results.getString(7)+" | $"+results.getFloat(8)+" | "+results.getString(9)); 
		      }
		      connection.close();
		    }catch(SQLException ex) {
		    	System.out.println("¡Error! verifique los datos de la consulta");
		    	System.out.println(ex.toString());
		    	ex.printStackTrace();
		    }
	}
	
	
	//Insert registrys
	public void insertEmployeed(){
		try {
		      Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/VAULT","root","");
		      
		      Statement query = connection.createStatement(); 
		      query.executeUpdate ("insert into VAULT.EMPLOYEES (FIRST_NAME,LAST_NAME,EMAIL,PHONE_NUMBER,HIRE_DATE,JOB_ID,SALARY,COMMISSION_PCT,MANAGER_ID,DEPARTMENT_ID) VALUES ('"+this.firstName+"','"+this.lastName+"','"+this.email+"','"+this.phoneNumber+"','"+this.hireDate+"',"+this.jobId+",'"+this.salary+"','"+this.commission+"',"+this.managerId+","+this.departmentId+");");
		      connection.close();
		    }catch(SQLException ex) {
		    	System.out.println("La insersion de datos da error");
		    	System.out.println(ex.toString());
				ex.printStackTrace();
		    }
	}
	
	//update registry
	public void updateEmployeed(){
		/*System.out.println(this.employeeId+" "+this.firstName+" "+this.lastName+" "+this.email+" "+this.phoneNumber+" "+this.hireDate+" "+this.jobId+" "+this.salary+" "+this.commission+" "+this.managerId+" "+this.departmentId);*/
		try {
		      Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/VAULT","root","");
		      String query="update VAULT.EMPLOYEES set MANAGER_ID=0 where EMPLOYEE_ID=0";

		      if( this.employeeId!=0 && !(this.firstName.equalsIgnoreCase("NULL"))) {
		    	  query = "update VAULT.EMPLOYEES set FIRST_NAME = '"+this.firstName+"' WHERE EMPLOYEE_ID = '"+this.employeeId+"';";
		      }else if( this.employeeId!=0 && !(this.lastName.equalsIgnoreCase("NULL"))) {
		    	  query = "update VAULT.EMPLOYEES set LAST_NAME= '"+this.lastName+"' WHERE EMPLOYEE_ID = '"+this.employeeId+"';";
		      }else if( this.employeeId!=0 && !(this.email.equalsIgnoreCase("NULL"))) {
		    	  query = "update VAULT.EMPLOYEES set EMAIL = '"+this.email+"' WHERE EMPLOYEE_ID = '"+this.employeeId+"';";
		      }else if( this.employeeId!=0 && !(this.phoneNumber.equalsIgnoreCase("NULL"))){
		    	  query = "update VAULT.EMPLOYEES set PHONE_NUMBER = '"+this.phoneNumber+"' WHERE EMPLOYEE_ID = '"+this.employeeId+"';";
		      }else if( this.employeeId!=0 && this.jobId!=0 ) {
		    	  query = "update VAULT.EMPLOYEES set JOB_ID = '"+this.jobId+"' WHERE EMPLOYEE_ID = '"+this.employeeId+"';";
		      }else if( this.employeeId!=0 && this.salary!=0 ) {
		    	  query = "update VAULT.EMPLOYEES set SALARY = '"+this.salary+"' WHERE EMPLOYEE_ID = '"+this.employeeId+"';";
		      }else if( this.employeeId!=0 && this.commission!=0 ) {
		    	  query = "update VAULT.EMPLOYEES set COMMISSION_PCT = '"+this.commission+"' WHERE EMPLOYEE_ID = '"+this.employeeId+"';";
		      }else if( this.employeeId!=0 && this.managerId!=0 ) {
		    	  query = "update VAULT.EMPLOYEES set MANAGER_ID = '"+this.managerId+"' WHERE EMPLOYEE_ID = '"+this.employeeId+"';";
		      }else if( this.employeeId!=0 && this.departmentId!=0 ) {
		    	  query = "update VAULT.EMPLOYEES set DEPARTMENT_ID = '"+this.departmentId+"' WHERE EMPLOYEE_ID = '"+this.employeeId+"';";
		      }
		      
		      Statement exec = connection.createStatement(); 
		      exec.executeUpdate (query);
		      connection.close();
		    }catch(SQLException ex) {
		    	System.out.println("No se pudo actualizar el registro");
		    	System.out.println(ex.toString());
				ex.printStackTrace();
		    }
	}
	
	//Delete registry
	public void deleteEmployeed(){
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/VAULT","root","");
		    Statement query = connection.createStatement(); 
		    int var=query.executeUpdate("delete from VAULT.EMPLOYEES where EMPLOYEE_ID = "+this.employeeId+";");
		    if(var==1) {
		    	System.out.println("Se borro el registro");
		    }else{
				System.out.println("No se pudo eliminar el registro");
		    }
		    connection.close();
		}catch(SQLException ex){
	    	System.out.println(ex.toString());
			ex.printStackTrace();
		}
	}
}
