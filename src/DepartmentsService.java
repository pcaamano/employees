import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DepartmentsService {
	
	/*** ATTRIBUTES ***/
	//private int deparmentId;
	private String deparmentName;
	private int locationId;
	private int departmentId;
	
	
	
	/*** CONTRUCTORS ***/
	public DepartmentsService() {
		
	}
	/*** OVERLOADED CONSTRUCTOR ***/
	public DepartmentsService(String departmentName, int locationId) {
		this.deparmentName=departmentName;
		this.locationId=locationId;
	}
	
	public DepartmentsService(int departmentId) {
		this.departmentId=departmentId;
	}
	
	

	
	
	/*** METHODS ***/
	public void listDepartments() {
		try{
		    Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/VAULT","root","");
		    Statement statement = connection.createStatement(); 
		    ResultSet results = statement.executeQuery("select dep.DEPARTMENT_NAME,loc.CITY,loc.STATE_PROVINCE,co.COUNTRY_NAME from VAULT.DEPARTMENTS dep join VAULT.LOCATIONS loc join VAULT.COUNTRIES co on dep.LOCATION_ID = loc.LOCATION_ID and loc.COUNTRY_ID = co.COUNTRY_ID order by dep.DEPARTMENT_ID;");
		    while(results.next()==true) {
		    	System.out.println(results.getString(1)+" | "+results.getString(2)+" | "+results.getString(3)+" | "+results.getString(4));
		    }
		    connection.close();
		}catch(SQLException ex){
			System.out.println("Error de conexion a Base de datos.");
			System.out.println(ex.toString());
			ex.printStackTrace();
			
		}
	}
	
	public void createNewDepartment() {
		try {
		      Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/VAULT","root","");
		      
		      Statement query = connection.createStatement(); 
		      query.executeUpdate ("insert into VAULT.DEPARTMENTS (DEPARTMENT_NAME,LOCATION_ID) VALUES ('"+this.deparmentName+"',"+this.locationId+");");
		      connection.close();
		    }catch(SQLException ex) {
		    	System.out.println("La insersion de datos da error");
		    	System.out.println(ex.toString());
				ex.printStackTrace();
		    }
	}
	
	public void promSalary4Department() {
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/VAULT","root","");
			Statement statement = connection.createStatement(); 
		    ResultSet results = statement.executeQuery("select (sum(emp.SALARY)/count(emp.EMPLOYEE_ID)), count(emp.EMPLOYEE_ID), dep.DEPARTMENT_NAME, loc.CITY,loc.STATE_PROVINCE from VAULT.EMPLOYEES emp join VAULT.DEPARTMENTS dep join VAULT.LOCATIONS loc on emp.DEPARTMENT_ID = dep.DEPARTMENT_ID and dep.LOCATION_ID = loc.LOCATION_ID group by dep.DEPARTMENT_ID,loc.LOCATION_ID;");
		    while(results.next()==true) {
		    	System.out.println("Salario Promedio: $"+results.getFloat(1)+" | Cantidad Empleados: "+results.getInt(2)+" | Departamento: "+results.getString(3)+" | Ciudad: "+results.getString(4)+" | Provincia: "+results.getString(5));
		    }
		    connection.close();
		}catch(SQLException ex) {
			System.out.println("No se pudo realizar consulta");
	    	System.out.println(ex.toString());
			ex.printStackTrace();
		}
		
	}
	
	public void promSalary4Insert() {
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/VAULT","root","");
			Statement statement = connection.createStatement(); 
		    ResultSet results = statement.executeQuery("select (sum(emp.SALARY)/count(emp.EMPLOYEE_ID)), count(emp.EMPLOYEE_ID), dep.DEPARTMENT_NAME, loc.CITY,loc.STATE_PROVINCE from VAULT.EMPLOYEES emp join VAULT.DEPARTMENTS dep join VAULT.LOCATIONS loc on emp.DEPARTMENT_ID = dep.DEPARTMENT_ID and dep.LOCATION_ID = loc.LOCATION_ID where emp.DEPARTMENT_ID = "+this.departmentId+" group by dep.DEPARTMENT_ID,loc.LOCATION_ID;");
		    while(results.next()==true) {
		    	System.out.println("Salario Promedio: $"+results.getFloat(1)+" | Cantidad Empleados: "+results.getInt(2)+" | Departamento: "+results.getString(3)+" | Ciudad: "+results.getString(4)+" | Provincia: "+results.getString(5));
		    }
		    connection.close();
		}catch(SQLException ex) {
			System.out.println("No se pudo realizar consulta");
	    	System.out.println(ex.toString());
			ex.printStackTrace();
		}
	}
}
