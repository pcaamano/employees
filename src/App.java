
import java.util.Scanner;

public class App {
	
	public static void initMenu() {
		System.out.println("");
		System.out.print("¿Dese realizar otra operacion? (si | no): ");
	}
	
	public static void mainMenu() {
		System.out.println("Operaciones ");
		System.out.println("");
		System.out.println("1- Listar Empleados");
		System.out.println("2- Buscar empleado");
		System.out.println("3- Agregar Empleado");
		System.out.println("4- Actualizar datos de Empleado");
		System.out.println("5- Borrar Empleado");
		System.out.println("6- Consultar Departamentos");
		System.out.println("7- Agregar Departamento");
		System.out.println("8- Calcular promedio de sueldos por departamentos");
		System.out.println("9- Probar conexion a Base de Datos");
		System.out.println("");
		System.out.print("Ingrese numero de operacion: ");
	}
	
	public static void validOption(String data) {
		System.out.println("");
		System.out.print("¿Sabe el "+data+"? ( si | no ): ");
	}
	
	public static void catchData(String data) {
		System.out.println("");
		System.out.print("Ingrese el "+data+": ");
	}
	
	public static void updateMenu() {
		System.out.println("Operaciones ");
		System.out.println("");
		System.out.println("1- Nombre");
		System.out.println("2- Apellido");
		System.out.println("3- Email");
		System.out.println("4- Telefono");
		System.out.println("5- Id de trabajo");
		System.out.println("6- Salario");
		System.out.println("7- Comision");
		System.out.println("8- Id del Jefe");
		System.out.println("9- Id de Departamento");
		System.out.println("");
		System.out.print("Ingrese numero de operacion: ");
	}
	
	public static void main(String[] args) {
		
		/*** VARS ***/
		int option=0;
		String init = "si";
		String valid="";
		int idEmployee=0;
		String name="";
		String lastName="";
		String email="";
		String phone="";
		String job="";
		String department="";
		String hireDate="";
		int jobId=0;
		float salary=0;
		float commission=0;
		int managerId=0;
		int departmentId=0;
		String departmentName="";
		int locationId=0;
		
				
		
		/*** OBJECTS ***/
		@SuppressWarnings("resource")
		Scanner read=new Scanner(System.in);
		EmployeeService run1=new EmployeeService();
		
		
		
		while(init.equalsIgnoreCase("si")) {

			mainMenu();
			option=read.nextInt();
			
			switch(option) {
				case 1:
					run1.listEmployees();
					break;
				case 2:
					validOption("id empleado");
					valid=read.next();
					if(valid.equalsIgnoreCase("si")) {
						catchData("id empleado");
						idEmployee=read.nextInt();
					}else {
						idEmployee=0;
						validOption("nombre");
						valid=read.next();
						if(valid.equalsIgnoreCase("si")) {
							catchData("nombre");
							name=read.next();
						}
						
						validOption("apellido");
						valid=read.next();
						if(valid.equalsIgnoreCase("si")) {
							catchData("apellido");
							lastName=read.next();
						}
						
						validOption("email");
						valid=read.next();
						if(valid.equalsIgnoreCase("si")) {
							catchData("email");
							email=read.next();
						}
						
						validOption("telefono");
						valid=read.next();
						if(valid.equalsIgnoreCase("si")) {
							catchData("telefono");
							phone=read.next();
						}
						
						validOption("puesto");
						valid=read.next();
						if(valid.equalsIgnoreCase("si")) {
							catchData("puesto");
							job=read.next();
						}
						
						validOption("departamento");
						valid=read.next();
						if(valid.equalsIgnoreCase("si")) {
							catchData("departamento");
							department=read.next();
						}
					}
					
					EmployeeService run2=new EmployeeService(idEmployee,name,lastName,email,phone,job,department);
					run2.findEmployee();
					break;
				case 3:
					catchData("nombre");
					name=read.next();
				
					catchData("apellido");
					lastName=read.next();
					
					catchData("email");
					email=read.next();
					
					catchData("telefono");
					phone=read.next();
					
					catchData("fecha de ingreso (AAAA-MM-DD)");
					hireDate=read.next();
					
					catchData("Id del Puesto de Trabajo");
					jobId=read.nextInt();
					
					catchData("salario");
					salary=read.nextFloat();
				
					catchData("comision");
					commission=read.nextFloat();
					
					catchData("Id del manager");
					 managerId=read.nextInt(); 
				
					catchData("id del departamento");
					departmentId=read.nextInt();
					
					EmployeeService run3=new EmployeeService(name,lastName,email,phone,hireDate,jobId,salary,commission,managerId,departmentId);
					run3.insertEmployeed();
					DepartmentsService run3b=new DepartmentsService();
					run3b.promSalary4Insert();
					break;
				case 4:
					validOption("id empleado");
					valid=read.next();
					if(valid.equalsIgnoreCase("si")) {
						catchData("id empleado");
						idEmployee=read.nextInt();
						
						updateMenu();
						option=read.nextInt();
						
						switch(option) {
							case 1:
								catchData("nombre");
								name=read.next();
								lastName="NULL";
								email="NULL";
								phone="NULL";
								jobId=0;
								salary=0;
								commission=0;
								managerId=0;
								departmentId=0;
								break;
							case 2:
								catchData("apellido");
								lastName=read.next();
								name="NULL";
								email="NULL";
								phone="NULL";
								jobId=0;
								salary=0;
								commission=0;
								managerId=0;
								departmentId=0;
								break;
							case 3:
								catchData("email");
								email=read.next();
								name="NULL";
								lastName="NULL";
								phone="NULL";
								jobId=0;
								salary=0;
								commission=0;
								managerId=0;
								departmentId=0;
								break;
							case 4:
								catchData("telefono");
								phone=read.next();
								name="NULL";
								lastName="NULL";
								email="NULL";
								jobId=0;
								salary=0;
								commission=0;
								managerId=0;
								departmentId=0;
								break;
							case 5:
								catchData("Id del Puesto de Trabajo");
								jobId=read.nextInt();
								name="NULL";
								lastName="NULL";
								email="NULL";
								phone="NULL";
								salary=0;
								commission=0;
								managerId=0;
								departmentId=0;
								break;
							case 6:
								catchData("salario");
								salary=read.nextFloat();
								name="NULL";
								lastName="NULL";
								email="NULL";
								phone="NULL";
								jobId=0;
								commission=0;
								managerId=0;
								departmentId=0;
								break;
							case 7:
								catchData("comision");
								commission=read.nextFloat();
								name="NULL";
								lastName="NULL";
								email="NULL";
								phone="NULL";
								jobId=0;
								salary=0;
								managerId=0;
								departmentId=0;
								break;
							case 8:
								catchData("Id del manager");
								managerId=read.nextInt();
								name="NULL";
								lastName="NULL";
								email="NULL";
								phone="NULL";
								jobId=0;
								salary=0;
								commission=0;
								departmentId=0;
								break;
							case 9:
								catchData("id del departamento");
								departmentId=read.nextInt();
								name="NULL";
								lastName="NULL";
								email="NULL";
								phone="NULL";
								jobId=0;
								salary=0;
								commission=0;
								managerId=0;
								break;
							default:
								System.out.println("");
								System.out.println("Opcion incorrecta.");
								break;
						}
						EmployeeService run4=new EmployeeService(idEmployee,name,lastName,email,hireDate,phone,jobId,salary,commission,managerId,departmentId);
						run4.updateEmployeed();
					}else {
						System.out.println("Verifique el id del empleado antes de realizar esta operacion");
					}
					break;
				case 5:
					catchData("id empleado");
					idEmployee=read.nextInt();
					EmployeeService run5=new EmployeeService(idEmployee);
					run5.deleteEmployeed();
					break;
				case 6:
					DepartmentsService run6=new DepartmentsService();
					run6.listDepartments();
					break;
				case 7:
					catchData("nombre del departamento");
					departmentName=read.next();
					
					catchData("Id de la localidad");
					locationId=read.nextInt();
					
					DepartmentsService run7=new DepartmentsService(departmentName,locationId);
					run7.createNewDepartment();
					break;
				case 8:
					DepartmentsService run8=new DepartmentsService();
					run8.promSalary4Department();
					break;
				case 9:
					run1.connectDB();
					break;
				default:
					System.out.println("Opcion incorrecta");
					break;
			}
			
			initMenu();
			init=read.next();
			
			if(init.equalsIgnoreCase("no")) {
				System.out.println("");
				System.out.println("Que tenga un buen dia :)");
			}
		}
		
		
	}

}
